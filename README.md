# utils

Common utilities suitable for use in Rust Builders.

## Status
[![Build Status](https://travis-ci.org/rust-builder/utils.svg?branch=master)](travis)
